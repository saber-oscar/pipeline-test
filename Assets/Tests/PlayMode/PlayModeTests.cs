using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class PlayModeTests
{
    [UnitySetUp]
    private IEnumerator UnitySetUp()
    {
        AsyncOperation sceneLoadOperation = EditorSceneManager.LoadSceneAsyncInPlayMode(
            $"{Application.dataPath}/Scenes/SampleScene.unity",
            new LoadSceneParameters(LoadSceneMode.Single, LocalPhysicsMode.None));
        sceneLoadOperation.allowSceneActivation = true;

        while (!sceneLoadOperation.isDone)
        {
            yield return null;
        }
    }

    [UnityTearDown]
    private IEnumerator TearDown()
    {
        yield return null;
    }

    [Test]
    public void BasicTestsSimplePasses()
    {
        Assert.AreEqual(66, 66);
    }
}
