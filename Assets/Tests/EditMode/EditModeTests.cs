using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using Newtonsoft.Json.Schema;

public class EditModeTests
{
    [Test]
    public void BasicTestsSimplePasses()
    {
        JSchema j = new JSchema();
        Assert.AreEqual(1, 1);
    }

    [UnityTest]
    public IEnumerator BasicTestsWithEnumeratorPasses()
    {
        yield return null;
        Assert.AreEqual(2, 2);
    }
}
