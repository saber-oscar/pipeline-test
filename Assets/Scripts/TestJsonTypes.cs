using Newtonsoft.Json.Schema;
using System;

#pragma warning disable IDE1006
[Serializable]
public class TestJsonTypes
{
    public JSchema schema {get; set;}
    public JSchemaType schemaType {get; set;}
    
    [Test]
    public void TestPrint()
    {
        Console.WriteLine("================================================================");
        Console.WriteLine("================================================================");
        Console.WriteLine("If you can see this, TestJsonTypes.cs is being run successfully.");
        Console.WriteLine("================================================================");
        Console.WriteLine("================================================================");    }
}

#pragma warning restore IDE1006
